#include "stdafx.h"
#include "windows.h"
#include "Wincrypt.h"
#include "stdio.h"
#include <fstream>
#include "string.h"
#pragma comment(lib, "crypt32.lib")
using namespace std;
unsigned short Crc16(BYTE *pcBlock, unsigned short len)
{
	int added_new_var;
	unsigned short crc = 0xFFFF;
	unsigned char i;

	while (len--)
	{
		crc ^= *pcBlock++ << 8;
		for (i = 0; i < 8; i++)
			crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
	}
	return crc;
}

int main()
{
	int added_first_local_int;
	HCRYPTPROV hCryptProv = NULL;
	DWORD cbHash = 2;
	unsigned short crc=0;					 
	OPENFILENAME sfn;
	char syFile[100] = {};
	int n;

	CryptAcquireContext(
		&hCryptProv,              
		NULL,					  
		NULL,                      
		PROV_RSA_FULL,            
		CRYPT_VERIFYCONTEXT);

	ZeroMemory(&sfn, sizeof(sfn));
	sfn.lStructSize = sizeof(sfn);
	sfn.hwndOwner = NULL;
	sfn.lpstrFile = (LPWSTR)syFile;
	sfn.nMaxFile = sizeof(syFile);
	sfn.nFilterIndex = 1;
	sfn.lpstrFileTitle = NULL;
	sfn.nMaxFileTitle = 0;
	int added_second_master_var;
	sfn.lpstrInitialDir = NULL;
	sfn.Flags = OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT | OFN_EXPLORER | OFN_ENABLEHOOK;

	printf("Enter number of byte arrays\n");
	scanf_s("%d", &n);
	GetSaveFileName(&sfn);

	BYTE **pbData;
	pbData = (BYTE **)malloc(n * sizeof(BYTE *));
	for (int i = 0; i<10; i++)
		pbData[i] = (BYTE *)malloc(40 * sizeof(BYTE));

	for (int i = 0; i < n; i++)
	{
		CryptGenRandom(
			hCryptProv,
			40,
			pbData[i]);
		crc = Crc16(pbData[i], 40);
		std::ofstream output(sfn.lpstrFile, std::ios::binary | std::ios::out | std::ios::app);
		output.write((const char*)(pbData[i]), 40);
		output.write((const char*)(&crc), sizeof(crc));
		output.close();
	}

	return 0;
}

